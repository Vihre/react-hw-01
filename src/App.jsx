import React, { Component } from 'react';

import { Button } from "./Components/Button"
import { ModalRegistration } from "./Components/ModalRegistration"

class App extends Component {
   state = {
      registrationModal: false,
      blankModal: false,
   }

   openRegistrationModal = () => {
      this.setState((prevState) => {
         return {
            ...prevState,
            registrationModal: !prevState.registrationModal
         }
      })
   }

   openBlankModal = () => {
      this.setState((prevState) => {
         return {
            ...prevState,
            blankModal: !prevState.blankModal
         }
      })
   }


   render() {
      const { registrationModal, blankModal } = this.state
      return (
         <div>
            <Button
               btnClass={"button"}
               backgroundColor={"blue"}
               openModalReg={this.openRegistrationModal}
               text={'Open first modal'}
            />
            <Button
               btnClass={"button"}
               backgroundColor={"green"}
               openModalReg={this.openBlankModal}
               text={'Open second modal'}
            />

            {registrationModal && <ModalRegistration
               Btn={true}
               closeModal={this.openRegistrationModal}
               headerRegModal={"Do you want to delete this file?"}
               textRegModal={'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?'}
               actions={<div className="button-wrapper">
                  <Button
                     btnClass={"btn"}
                     text={'ok'}
                     openModalReg={this.openRegistrationModal}
                  />
                  <Button
                     btnClass={"btn"}
                     text={'cancel'}
                     openModalReg={this.openRegistrationModal}
                  />
               </div>}
            />}

            {blankModal && <ModalRegistration
               XBtn={true}
               closeModal={this.openBlankModal}
               headerRegModal={"Request form"}
               textRegModal={'Enter your problem and we will mail you'}
               actions={<div className="button-wrapper" >
                  <Button
                     btnClass={"btn"}
                     text={'ok'}
                     openModalReg={this.openBlankModal}
                  />
                  <Button
                     btnClass={"btn"}
                     text={'cancel'}
                     openModalReg={this.openBlankModal}
                  />
               </div>}
            />}
         </div>
      )
   }
}

export default App;
