import { Component } from "react";
import "./Button.scss"

class Button extends Component {
   render() {
      const { backgroundColor, text, openModalReg, btnClass } = this.props

      return (
         <button className={btnClass} style={{ background: backgroundColor }} onClick={openModalReg}>{text}</button>
      )
   }
}

export default Button
